# Bevy Egui Example

A very clean example on how to use Egui in Bevy!



# Usage:

Run Without Styling: `cargo run --`
Run With Styling: `cargo run -- --styling`

Note: If you are on Linux/MacOS, you can use the `-F bdl` flag too! (`cargo run -F bdl --` or `cargo run -F bdl -- --styling`)
