/* README

If you want to see how the UI is being built, look at draw_ui_system().
Also, note that the system is being added as an Update system vs a Startup system.

Useful Links:
    Styling:
        egui::style::Visuals -> https://docs.rs/egui/latest/egui/style/struct.Visuals.html
        Widget Styling -> https://docs.rs/egui/latest/src/egui/style.rs.html#1255-1298

*/

mod cli;

use bevy::prelude::*;
use bevy_egui::*;
use clap::Parser;

#[derive(Component)]
struct LittleCube;

fn main() {
    let args = cli::Cli::parse();

    let mut app = App::new();

    app.add_plugins(DefaultPlugins.set(
        WindowPlugin {
            primary_window: Some(Window {
                title: "Bevy Egui Example".into(),
                ..default()
            }),
            ..default()
        }
    ));

    app.add_plugins(EguiPlugin);

    app.add_systems(Startup, spawn_camera_system);
    app.add_systems(Startup, spawn_cube_system);
    app.add_systems(Startup, spawn_light_system);
    app.add_systems(Update, draw_ui_system); // This is what you want to focus on. Note the 'Update' too.

    app.insert_resource(ClearColor(Color::rgb(0.5, 0.5, 0.5)));
    app.insert_resource(args);

    app.run();
}

// This is what you should focus on in this example.
fn draw_ui_system(
    mut contexts: EguiContexts,
    mut query: Query<&mut Transform, With<LittleCube>>,
    time: Res<Time>,
    args: Res<cli::Cli>,
) {
    // Storing the ctx as a variable so we can do some UI styling if we want to!
    let ctx = contexts.ctx_mut();

    // UI Styling (Optional)
    if args.styling {
        let old_style = ctx.style().visuals.clone();

        style_ctx(ctx, &old_style); // This is a function defined below this one. (Below: draw_ui_system, Called: style_ctx)
    }
    // ^ End of optional UI styling.

    // Actual UI Creation
    egui::Window::new("Very Epic Window").show(ctx, |ui| {
        ui.horizontal(|ui| {
            ui.label("Rotate Cube By Clicking:");

            if ui.button("Rotate Cube").clicked() {
                for mut i in query.iter_mut() {
                    i.rotate(Quat::from_rotation_y(20.0 * time.delta_seconds()));
                }
            }
        });

        ui.horizontal(|ui| {
            ui.label("Cube X Scale:");

            ui.add(egui::Slider::new(&mut query.single_mut().scale.x, 1.0..=10.0));
        });

        ui.horizontal(|ui| {
            ui.label("Cube Y Scale:");

            ui.add(egui::Slider::new(&mut query.single_mut().scale.y, 1.0..=10.0));
        });

        ui.horizontal(|ui| {
            ui.label("Cube Z Scale:");

            ui.add(egui::Slider::new(&mut query.single_mut().scale.z, 1.0..=10.0));
        });
    });
}

// All the styling code was moved into this function just to keep things tidy.
fn style_ctx(ctx: &mut egui::Context, old_style: &egui::style::Visuals) {
    ctx.set_visuals(egui::style::Visuals {
        override_text_color: Some(egui::Color32::from_rgba_unmultiplied(0, 0, 0, 255)),
        window_shadow: egui::epaint::Shadow {
            color: egui::Color32::TRANSPARENT,
            ..old_style.window_shadow
        },
        window_fill: egui::Color32::from_rgba_unmultiplied(255, 80, 0, 240),
        widgets: egui::style::Widgets {
            active: egui::style::WidgetVisuals {
                weak_bg_fill: egui::Color32::from_rgb(205, 30, 0),
                bg_fill: egui::Color32::from_rgb(205, 30, 0),
                bg_stroke: egui::Stroke::new(1.0, egui::Color32::from_rgb(0, 0, 0)),
                fg_stroke: egui::Stroke::new(2.0, egui::Color32::from_rgb(0, 0, 0)),
                rounding: egui::Rounding::same(1.0),
                expansion: 1.0,
            },
            inactive: egui::style::WidgetVisuals {
                weak_bg_fill: egui::Color32::from_rgb(0, 250, 255),
                bg_fill: egui::Color32::from_rgb(0, 250, 255),
                bg_stroke: egui::Stroke::new(1.0, egui::Color32::from_rgb(0, 0, 0)),
                fg_stroke: egui::Stroke::new(2.0, egui::Color32::from_rgb(0, 0, 0)),
                rounding: egui::Rounding::same(1.0),
                expansion: 1.0,
            },
            hovered: egui::style::WidgetVisuals {
                weak_bg_fill: egui::Color32::from_rgb(245, 70, 0),
                bg_fill: egui::Color32::from_rgb(245, 70, 0),
                bg_stroke: egui::Stroke::new(1.0, egui::Color32::from_rgb(0, 0, 0)),
                fg_stroke: egui::Stroke::new(2.0, egui::Color32::from_rgb(0, 0, 0)),
                rounding: egui::Rounding::same(1.0),
                expansion: 1.0,
            },
            ..egui::style::Widgets::light()
        },
        ..*old_style
    });
}

fn spawn_cube_system(mut cmds: Commands, mut meshes: ResMut<Assets<Mesh>>, mut materials: ResMut<Assets<StandardMaterial>>) {
    cmds.spawn((
        PbrBundle {
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
            material: materials.add(StandardMaterial {
                base_color: Color::rgb(0.2, 0.7, 1.0),
                ..default()
            }),
            ..default()
        },

        LittleCube,
    ));
}

fn spawn_light_system(mut cmds: Commands) {
    cmds.spawn(
        PointLightBundle {
            point_light: PointLight {
                color: Color::rgb(1.0, 1.0, 1.0),
                intensity: 500.0,
                ..default()
            },
            transform: Transform::from_xyz(4.0, 5.0, 6.0),
            ..default()
        }
    );
}

fn spawn_camera_system(mut cmds: Commands) {
    cmds.spawn(Camera3dBundle {
        transform: Transform::from_xyz(5.0, 5.0, 5.0)
            .looking_at(Vec3::new(0.0, 0.0, 0.0), Vec3::Y),
        ..default()
    });
}
