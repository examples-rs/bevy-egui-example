use clap::Parser;
use bevy::prelude::*;

#[derive(Parser, Resource)]
pub struct Cli {
    #[clap(short, long)]
    /// Enable styling in example
    pub styling: bool,
}
